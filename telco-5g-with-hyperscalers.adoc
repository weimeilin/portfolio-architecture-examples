= Telco 5G with Hyperscalers
 Rimma Iontel linkedin.com/in/rimma-iontel-5267004, Ishu Verma  @ishuverma, William Henry @ipbabble,
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:description: 5G is the latest evolution of wireless mobile technology. It can deliver a number of services from the network edge
:Keywords: Telco 5G, OpenShift, Ansible, Hybrid Cloud, Linux, Automation, Mobile Broadband
:toc: left
:toclevels: 5


5G is the latest evolution of wireless mobile technology. In this portfolio architecture we'll discuss a 5G solution
built with open source technologies at core, that can work across any hyperscaler.

====
*Telco 5G with Hyperscalers*

. Cloud native, disaggregated, scalable and  agile 5G core
. Custom networks with unique characteristics on a shared infrastructure
. Cost effective, highly scalable, automatable with minimal ROI risk
====

*Use case:* Build an adaptable, on-demand infrastructure services for 5G Core that can deliver across diverse use
cases with minimal CAPEX and OPEX.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/telco-5g-core-hyperscalers-marketing-slide.png[alt="High level view of 5G core solution with hyperscalers", width=700]
--


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/telco5GC-generic-7-ld.png[alt="Generic common architectural elements of 5G solution for conceptual guidance", width=700]
--

== The technology
The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy.
OpenShift enables 5G core by supporting functionalities and operational features like automated deployment, intelligent
workload placement, dynamic scaling, hitless upgrades, and self healing.

https://www.redhat.com/en/technologies/management/ansible?intcmp=7013a00000318EWAAY[*Red Hat Ansible Automation Platform*] is a foundation for building and operating automation across an organization.
The platform includes all the tools needed to implement enterprise-wide automation. It enables cluster and network
operations administrators to automate deployment of functional components across hybrid cloud.

https://www.redhat.com/en/technologies/management/advanced-cluster-management?intcmp=7013a00000318EWAAY[*Red Hat Advanced Cluster Management*] for Kubernetes controls clusters and applications from a single console, with
built-in security policies. Extend the value of Red Hat OpenShift by deploying apps, managing multiple clusters, and
enforcing policies across multiple clusters at scale.

https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Data Foundations*] is software-defined storage for containers. Engineered as the data and storage
services platform for Red Hat OpenShift, Red Hat OpenShift Data Foundation helps teams develop and deploy applications
quickly and efficiently across clouds. Its used for persistent storage across  clusters across hybrid cloud.
====

Conceptually, the 5G solution stack with hyperscalers can be categorized into:

* Infrastructure

* Application Platform

* Applications

* Platform Management and Application Orchestration

Infrastructure provides necessary compute, network and storage resources to the application platform. Application platform
accommodates the applications with declarative desired state consistency with facilities to perform scaling, healing and monitoring.
Applications provide the business logic they are aimed to deliver in a homogenous performant way (i.e. wider, stronger, faster 5G).
Management and orchestration allows dynamic scaling of end-to-end 5G solution, across multiple locations with automation.


== Telco 5G with Hyperscalers
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/telco5GC-generic-7-sd.png[alt="Solution topology of 5G solution with functional components", width=700]
--
The messages from endpoints and Radio Access Network (RAN) are routed to the OpenShift clusters running on AWS and on
AWS Outposts in user plane/multi-access edge.

User Plane Function (UPF) handles packet processing and traffic aggregation of user traffic.

Access and Mobility Management Function (AMF) and Session Management Function (SMF) are part of the control plane. AMF handles
connections and mobility management tasks while SMF handles session management. AMF receives connection and session-related info
from the end devices, passing the session info to SMF, which establishes sessions by using UPF.

Policy Control Function (PCF) provides a framework for creating policies to be consumed by the other control plane network functions.

Authentication Server Function (AUSF) provides authentication and Unified Data Management (UDM) ensuring user
identification, authorization, and subscription management.

The following components provide the supplementary services:

* Network Repository Function (NRF) is used by AMF to select the correct SMF out of the pool.

* NRF and Network Slice Selection Function (NSSF) work together to support network slicing capabilities.

* Network Exposure Function (NEF) exposes 5G services and resources so third-party apps can more securely access 5G services.

* Application Function (AF) exposes an application layer for interacting with 5G network resources, retrieving resource info from
  PCF and exposing them.

The management service is provided by the Element Management System/Container Network Function Manager (EMS/CNFM) is responsible for
the application’s life cycle: provisioning, configuration, scaling, updates, etc. This component would be application specific, and
depending on the vendor implementation, would interact with the platform and the application over open or proprietary API
interfaces. This component is optional and its functionality might be rolled into the Orchestrator or implemented using Operators.

OpenShift Service Mesh is used for service discovery and exposure, and as a mechanism for specialized network handling, certificate
management, etc.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/telco5GC-generic.drawio[[Open Diagrams]]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/telco-5g-with-hyperscalers.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
