= Telco 5G: Radio Access Networks
 Ishu Verma  @ishuverma, Hanen Garcia
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:description: 5G RAN modernization by taking advantage of latest cloud technology
:Keywords: Telco 5G, OpenShift, Ansible, Hybrid Cloud, Linux, Automation, Mobile Broadband, Radio Access Network
:toc: left
:toclevels: 5


Radio access network (RAN) is the part of a mobile network that connects end-user devices, like smartphones, to the cloud for telecommunications network operators and RANs accounts for significant overall network expenses and key focus areas as more edge and 5G use cases emerge for telco customers.

====
*Telco 5G: Radio Access Networks*

. Cloud native, disaggregated, scalable and agile Radio Access Network
. Custom networks with unique characteristics on a shared infrastructure
. Cost effective, security conscious and use case adoptable
====

*Use case:* The digital transformation of mobile networks is accelerating and cloudification is increasing. Following the core network, radio access network (RAN) solutions are now taking advantage of the benefits of cloud computing.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/telco-ran-marketing-slide.png[alt="High level view of Radio Access Network", width=700]
--

The traditional RAN solution is getting disaggregated by:

* Splitting the remote radio head and baseband unit → radio unit, distributed unit and centralized unit (user and control plane)
* Decoupling the dedicated hardware → COTS and general purpose hardware available with CSPs

Using this approach:

* Uses less (and less expensive) hardware
* Increases flexibility
* Provides the ability to spin workloads up and down with minimal effort
* Allows resources to be scaled elastically to address changing network demands
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/telco-ran-ld.png[alt="Conceptual view of Radio Access Network deployed at various locations", width=700]
--

== The technology

The following technology was chosen for this solution:

====
*Red Hat OpenShift* is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy. It
provides a consistent application platform to manage hybrid cloud, multicloud, and edge deployments.

*Red Hat Smart Management* combines flexible and powerful infrastructure management capabilities with the
ability to execute remediation plans. It helps you more securely manage any environment supported by Red Hat Enterprise
Linux, from physical machines to hybrid multiclouds.

*Red Hat Advanced Cluster Management* for Kubernetes controls clusters and applications from a single console, with
built-in security policies. Extend the value of Red Hat OpenShift by deploying apps, managing multiple clusters, and
enforcing policies across multiple clusters at scale.

*Red Hat Quay* is a private container registry that stores, builds, and deploys container images. It analyzes your
images for security vulnerabilities, identifying potential issues that can help you mitigate security risks.

*Red Hat Identity Management* provides a centralized and unified way to manage identity stores, authentication,
policies, and authorization policies in a Linux-based domain.

*Red Hat OpenShift Data Foundations* is software-defined storage for containers. Engineered as the data and storage
services platform for Red Hat OpenShift, Red Hat OpenShift Data Foundation helps teams develop and deploy applications
quickly and efficiently across clouds.

*Red Hat Enterprise Linux* is the world’s leading enterprise Linux platform. It’s an open source operating system
(OS). It’s the foundation from which you can scale existing apps—and roll out emerging technologies—across bare-metal,
virtual, container, and all types of cloud environments.
====

== Telco 5G Radio Access Networks
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/telco-ran-sd.png[alt="Network topology of network components", width=700]

By decoupling RAN software from the underlying hardware platforms, commodity hardware platforms can be used for deploying RAN components like CU (Central Units) and the DU (Distributed Units). This architecture supports the Open Radio Access Network deployment scenario, where the Distributed Units are located at the edge sites and Central Units located at the regional data center.
The Radio Unit is deployed at the cell site.

The regional data center hosts operators needed for deployment and operations of various infrastructure components like Intel wireless FEC accelerator, SR-IOV NIC accelerator, PTP, storage, logging, cluster management, and GitOps.

The central data center hosts 5G core components and other management functions (cluster management, code/configuration repository) etc.

O-RAN alliance defines the usage of the interfaces between:

* Orchestrator and RAN components – A1 interface.
* RIC (RAN Intelligent Controller) and CU/DU – E2 Interface.
* CU-CP (control plane) and CU-UP (user plane) – E1 Interface.
* CU-DU – F1 interface.
* DU-RU (radio unit) – Open FrontHaul.
* Orchestrator and Cloud Platform (O-Cloud) – O2 Interface.

--
== Radio Access Networks management and orchestration
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/telco-ran-mgmt-sd.png[alt="Management and Orchestration of RAN components", width=700]
--

As service providers deploy applications across multiple sites, new operational & business challenges arise. Some of those challenges include the following:

* Apps are difficult to manage at scale and error prone.
* Inconsistency with security controls across environments.
* Overwhelming to verify components.
* Difficulty in managing configurations, policies, and compliance.

GitOps is the preferred solution to manage such complex operational scenarios.

GitOps Operator enables management of C-RAN and D-RAN components with GitOps workflows across multiple sites.

The various components of the RAN solution are orchestrated in a standardized manner using Kubernetes primitives and ACM. The event streaming data for various metrics and logs is enabled with Kafka.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/telco-ran-pb.drawio[[Open Diagrams]]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/telco-radio-access-networks.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
